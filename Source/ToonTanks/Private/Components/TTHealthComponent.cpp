// Copyright (C) 2021 Oleksandr Manenko. All rights reserved.

#include "Components/TTHealthComponent.h"

#include "Kismet/GameplayStatics.h"
#include "Logging/LogMacros.h"
#include "Logging/LogVerbosity.h"
#include "World/TTGameModeBase.h"

UTTHealthComponent::UTTHealthComponent()
{
    PrimaryComponentTick.bCanEverTick = false;
}

void UTTHealthComponent::BeginPlay()
{
    Super::BeginPlay();

    Health      = DefaultHealth;
    GameModeRef = Cast<ATTGameModeBase>( UGameplayStatics::GetGameMode( this ) );

    GetOwner()->OnTakeAnyDamage.AddDynamic( this, &UTTHealthComponent::TakeDamage );
}

void UTTHealthComponent::TakeDamage(
    AActor *           DamageActor,
    float              DamageAmount,
    const UDamageType *DamageType,
    AController *      InstigatedBy,
    AActor *           DamageCauser )
{
    if ( DamageAmount == 0.f || Health <= 0.f ) {
        return;
    }

    Health = FMath::Clamp( Health - DamageAmount, 0.f, DefaultHealth );
    if ( Health <= 0.f ) {
        if ( GameModeRef ) {
            GameModeRef->ActorDied( GetOwner() );
        } else {
            UE_LOG( LogTemp, Warning, TEXT( "Health Component has no reference to a Game Mode" ) );
        }
    }
}
