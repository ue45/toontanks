// Copyright (C) 2021 Oleksandr Manenko. All rights reserved.

#include "Pawns/PawnBase.h"

#include "Actors/ProjectileBase.h"
#include "Components/CapsuleComponent.h"
#include "Components/SceneComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/TTHealthComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Logging/LogMacros.h"
#include "Logging/LogVerbosity.h"

APawnBase::APawnBase()
{
    PrimaryActorTick.bCanEverTick = true;

    Capsule       = CreateDefaultSubobject<UCapsuleComponent>( TEXT( "Capsule Collider" ) );
    RootComponent = Capsule;

    BaseMesh = CreateDefaultSubobject<UStaticMeshComponent>( TEXT( "Base Mesh" ) );
    BaseMesh->SetupAttachment( RootComponent );

    TurretMesh = CreateDefaultSubobject<UStaticMeshComponent>( TEXT( "Turret Mesh" ) );
    TurretMesh->SetupAttachment( BaseMesh );

    ProjectileSpawnPoint = CreateDefaultSubobject<USceneComponent>( TEXT( "Projectile Spawn Point" ) );
    ProjectileSpawnPoint->SetupAttachment( TurretMesh );

    Health = CreateDefaultSubobject<UTTHealthComponent>( TEXT( "Health" ) );
}

void APawnBase::RotateTurret( FVector LookAtTarget )
{
    // Update TurretMesh rotation to face towards to the LookAtTarget

    FVector TurretMeshLocation{ TurretMesh->GetComponentLocation() };
    FVector LookAtTargetCleaned{ LookAtTarget.X, LookAtTarget.Y, TurretMeshLocation.Z };

    FRotator TurretRotation{ ( LookAtTargetCleaned - TurretMeshLocation ).Rotation() };

    TurretMesh->SetWorldRotation( TurretRotation );
}

void APawnBase::Fire()
{
    // Get ProjectileSpawnPoint Location && Rotation -> Spawn Projectile class at location firing towards rotation.
    if ( ProjectileClass ) {
        FVector  SpawnLocation = ProjectileSpawnPoint->GetComponentLocation();
        FRotator SpawnRotation = ProjectileSpawnPoint->GetComponentRotation();

        auto *Projectile = GetWorld()->SpawnActor<AProjectileBase>( ProjectileClass, SpawnLocation, SpawnRotation );
        Projectile->SetOwner( this );
    } else {
        UE_LOG( LogTemp, Warning, TEXT( "Actor '%s' tried to fire but it has no Projectile Class set" ), *GetName() );
    }
}

void APawnBase::HandleDestruction()
{
    UGameplayStatics::SpawnEmitterAtLocation( this, DeathParticle, GetActorLocation() );
    UGameplayStatics::SpawnSoundAtLocation( this, DeathSound, GetActorLocation() );

    GetWorld()->GetFirstPlayerController()->ClientStartCameraShake( DeathShake );
}
