// Copyright (C) 2021 Oleksandr Manenko. All rights reserved.

#include "Pawns/PawnTurret.h"

#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"
#include "Pawns/PawnTank.h"

void APawnTurret::BeginPlay()
{
    Super::BeginPlay();

    GetWorld()
        ->GetTimerManager()
        .SetTimer( FireRateTimerHandle, this, &APawnTurret::CheckFireCondition, FireRate, true );

    PlayerPawn = Cast<APawnTank>( UGameplayStatics::GetPlayerPawn( this, 0 ) );
}

void APawnTurret::Tick( float DeltaTime )
{
    Super::Tick( DeltaTime );

    if ( IsPlayerInFireRange() ) {
        RotateTurret( PlayerPawn->GetActorLocation() );
    }
}

void APawnTurret::HandleDestruction()
{
    Super::HandleDestruction();

    Destroy();
}

void APawnTurret::CheckFireCondition()
{
    if ( IsPlayerInFireRange() ) {
        Fire();
    }
}

bool APawnTurret::IsPlayerInFireRange()
{
    return PlayerPawn && PlayerPawn->GetIsAlive() && GetDistanceTo( PlayerPawn ) <= FireRange;
}
