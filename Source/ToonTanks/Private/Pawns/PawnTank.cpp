// Copyright (C) 2021 Oleksandr Manenko. All rights reserved.

#include "Pawns/PawnTank.h"

#include "Camera/CameraComponent.h"
#include "Components/AudioComponent.h"
#include "Components/InputComponent.h"
#include "Engine/World.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "Kismet/GameplayStatics.h"

APawnTank::APawnTank()
{
    SpringArm = CreateDefaultSubobject<USpringArmComponent>( TEXT( "Spring Arm" ) );
    SpringArm->SetupAttachment( RootComponent );

    Camera = CreateDefaultSubobject<UCameraComponent>( TEXT( "Camera" ) );
    Camera->SetupAttachment( SpringArm );
}

void APawnTank::BeginPlay()
{
    Super::BeginPlay();

    PlayerController = Cast<APlayerController>( GetController() );
    EngineAudio      = UGameplayStatics::SpawnSound2D( this, EngineSound );
    if ( EngineAudio ) {
        EngineAudio->Stop();
    }
}

void APawnTank::Tick( float DeltaTime )
{
    Super::Tick( DeltaTime );

    Turn();
    Move();

    if ( PlayerController ) {
        FHitResult TraceHitResult;
        PlayerController->GetHitResultUnderCursor( ECC_Visibility, false, TraceHitResult );

        FVector HitLocation = TraceHitResult.ImpactPoint;
        RotateTurret( HitLocation );
    }
}

void APawnTank::HandleDestruction()
{
    Super::HandleDestruction();

    OnStopMoving();
    bIsAlive = false;

    SetActorHiddenInGame( true );
    SetActorTickEnabled( false );
}

bool APawnTank::GetIsAlive() const
{
    return bIsAlive;
}

void APawnTank::SetupPlayerInputComponent( UInputComponent *PlayerInputComponent )
{
    Super::SetupPlayerInputComponent( PlayerInputComponent );

    PlayerInputComponent->BindAxis( "MoveForward", this, &APawnTank::CalculateMoveInput );
    PlayerInputComponent->BindAxis( "Turn", this, &APawnTank::CalculateTurnInput );
    PlayerInputComponent->BindAction( "Fire", IE_Pressed, this, &APawnTank::Fire );

    //    PlayerInputComponent->BindAction( "Move", IE_Pressed, this, &APawnTank::StartMoving );
    // PlayerInputComponent->BindAction( "Move", IE_Released, this, &APawnTank::StopMoving );
}

void APawnTank::CalculateMoveInput( float Value )
{
    MoveDirection = FVector{ Value * MoveSpeed * GetWorld()->DeltaTimeSeconds, 0, 0 };
}

void APawnTank::CalculateTurnInput( float Value )
{
    float    RotateAmount = Value * TurnSpeed * GetWorld()->DeltaTimeSeconds;
    FRotator Rotation{ 0, RotateAmount, 0 };

    TurnDirection = FQuat{ Rotation };
}

void APawnTank::OnStartMoving()
{
    bMoving = true;

    if ( EngineAudio && !EngineAudio->IsPlaying() ) {
        EngineAudio->Play();
    }
}

void APawnTank::OnStopMoving()
{
    bMoving = false;

    if ( EngineAudio && EngineAudio->IsPlaying() ) {
        EngineAudio->Stop();
    }
}

bool APawnTank::HasMovement()
{
    bool bNewRotation = !FMath::IsNearlyZero( TurnDirection.Rotator().Yaw );
    bool bNewMovement = !MoveDirection.IsNearlyZero();

    return bNewRotation || bNewMovement;
}

void APawnTank::UpdateMovement()
{
    if ( bMoving && !HasMovement() ) {
        // Was moving but there is no movement now
        OnStopMoving();
    } else if ( !bMoving && HasMovement() ) {
        // Was not moving but there is a movement now
        OnStartMoving();
    }
}

void APawnTank::Move()
{
    AddActorLocalOffset( MoveDirection, true );
    UpdateMovement();
}

void APawnTank::Turn()
{
    AddActorLocalRotation( TurnDirection, true );
    UpdateMovement();
}
