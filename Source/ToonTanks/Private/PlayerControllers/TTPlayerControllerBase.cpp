// Copyright (C) 2021 Oleksandr Manenko. All rights reserved.

#include "PlayerControllers/TTPlayerControllerBase.h"

void ATTPlayerControllerBase::SetPlayerEnabledState( bool bEnabled )
{
    if ( bEnabled ) {
        GetPawn()->EnableInput( this );
    } else {
        GetPawn()->DisableInput( this );
    }

    bShowMouseCursor = bEnabled;
}
