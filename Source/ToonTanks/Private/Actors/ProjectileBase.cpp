// Copyright (C) 2021 Oleksandr Manenko. All rights reserved.

#include "Actors/ProjectileBase.h"

#include "Components/StaticMeshComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"

AProjectileBase::AProjectileBase()
{
    PrimaryActorTick.bCanEverTick = false;

    ProjectileMesh = CreateDefaultSubobject<UStaticMeshComponent>( TEXT( "Projectile Mesh" ) );
    ProjectileMesh->OnComponentHit.AddDynamic( this, &AProjectileBase::OnHit );
    RootComponent = ProjectileMesh;

    ParticleTrail = CreateDefaultSubobject<UParticleSystemComponent>( TEXT( "Particle Trail" ) );
    ParticleTrail->SetupAttachment( RootComponent );

    ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>( TEXT( "Projectile Movement" ) );
    ProjectileMovement->InitialSpeed = MovementSpeed;
    ProjectileMovement->MaxSpeed     = MovementSpeed;

    InitialLifeSpan = 3.f;
}

void AProjectileBase::BeginPlay()
{
    Super::BeginPlay();

    UGameplayStatics::PlaySoundAtLocation( this, LaunchSound, GetActorLocation() );
}

void AProjectileBase::OnHit(
    UPrimitiveComponent *HitComponent,
    AActor *             OtherActor,
    UPrimitiveComponent *OtherComponent,
    FVector              NormalImpulse,
    const FHitResult &   Hit )
{
    AActor *ThisOwner = GetOwner();
    if ( !ThisOwner ) {
        return;
    }

    // If the other actor exists and IS NOT self or owner, then apply the damage
    if ( OtherActor && OtherActor != this && OtherActor != ThisOwner ) {
        UGameplayStatics::ApplyDamage(
            OtherActor,
            DamageAmount,
            ThisOwner->GetInstigatorController(),
            this,
            DamageType );

        UGameplayStatics::SpawnEmitterAtLocation( this, HitParticle, GetActorLocation() );
        UGameplayStatics::PlaySoundAtLocation( this, HitSound, GetActorLocation() );

        GetWorld()->GetFirstPlayerController()->ClientStartCameraShake( HitShake );

        Destroy();
    }
}
