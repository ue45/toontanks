// Copyright (C) 2021 Oleksandr Manenko. All rights reserved.

#include "World/TTGameModeBase.h"

#include "Kismet/GameplayStatics.h"
#include "Pawns/PawnTank.h"
#include "Pawns/PawnTurret.h"
#include "PlayerControllers/TTPlayerControllerBase.h"

void ATTGameModeBase::BeginPlay()
{
    Super::BeginPlay();

    HandleGameStart();
}

void ATTGameModeBase::ActorDied( AActor *DeadActor )
{
    if ( DeadActor == PlayerTank ) {
        PlayerTank->HandleDestruction();
        HandleGameOver( false );

        if ( PlayerController ) {
            PlayerController->SetPlayerEnabledState( false );
        }

        return;
    }

    auto *Turret = Cast<APawnTurret>( DeadActor );
    if ( Turret ) {
        Turret->HandleDestruction();
        if ( --TargetTurrets <= 0 ) {
            HandleGameOver( true );
        }
    }
}

void ATTGameModeBase::HandleGameStart()
{
    TargetTurrets    = GetTargetTurretsCount();
    PlayerTank       = Cast<APawnTank>( UGameplayStatics::GetPlayerPawn( this, 0 ) );
    PlayerController = Cast<ATTPlayerControllerBase>( UGameplayStatics::GetPlayerController( this, 0 ) );

    GameStart();

    if ( PlayerController ) {
        PlayerController->SetPlayerEnabledState( false );

        FTimerHandle   PlayerEnableHandle;
        FTimerDelegate PlayerEnableDelegate = FTimerDelegate::CreateUObject(
            PlayerController,
            &ATTPlayerControllerBase::SetPlayerEnabledState,
            true );
        GetWorldTimerManager().SetTimer( PlayerEnableHandle, PlayerEnableDelegate, StartDelay, false );
    }
}

void ATTGameModeBase::HandleGameOver( bool PlayerWon )
{
    GameOver( PlayerWon );
}

int32 ATTGameModeBase::GetTargetTurretsCount()
{
    TArray<AActor *> Turrets;
    UGameplayStatics::GetAllActorsOfClass( this, APawnTurret::StaticClass(), Turrets );

    return Turrets.Num();
}
