// Copyright (C) 2021 Oleksandr Manenko. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"

#include "TTPlayerControllerBase.generated.h"

UCLASS()
class TOONTANKS_API ATTPlayerControllerBase: public APlayerController
{
    GENERATED_BODY()

public:
    void SetPlayerEnabledState( bool bEnabled );
};
