// Copyright (C) 2021 Oleksandr Manenko. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"

#include "TTGameModeBase.generated.h"

class APawnTank;
class APawnTurret;
class ATTPlayerControllerBase;

UCLASS()
class TOONTANKS_API ATTGameModeBase: public AGameModeBase
{
    GENERATED_BODY()

public:
    void ActorDied( AActor *DeadActor );

protected:
    void BeginPlay() override;

protected:
    UPROPERTY( EditDefaultsOnly, BlueprintReadOnly, Category = "Game Loop" )
    int32 StartDelay;

    UPROPERTY( EditDefaultsOnly, BlueprintReadOnly, meta = ( AllowPrivateAccess = "true" ) )
    int32 TargetTurrets = 0;

protected:
    UFUNCTION( BlueprintImplementableEvent )
    void GameStart();

    UFUNCTION( BlueprintImplementableEvent )
    void GameOver( bool PlayerWon );

private:
    void  HandleGameStart();
    void  HandleGameOver( bool PlayerWon );
    int32 GetTargetTurretsCount();

private:
    APawnTank *              PlayerTank;
    ATTPlayerControllerBase *PlayerController;
};
