// Copyright (C) 2021 Oleksandr Manenko. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Engine/EngineTypes.h"
#include "GameFramework/Actor.h"
#include "Particles/WorldPSCPool.h"

#include "ProjectileBase.generated.h"

class UParticleSystemComponent;
class UPrimitiveComponent;
class UProjectileMovementComponent;
class USoundBase;

UCLASS()
class TOONTANKS_API AProjectileBase: public AActor
{
    GENERATED_BODY()

public:
    AProjectileBase();

protected:
    void BeginPlay() override;

private: // Components -------------------------------------------------------------------------------------------------
    UPROPERTY( VisibleAnywhere, BlueprintReadonly, Category = "Components", meta = ( AllowPrivateAccess = true ) )
    UProjectileMovementComponent *ProjectileMovement;

    UPROPERTY( VisibleAnywhere, BlueprintReadonly, Category = "Components", meta = ( AllowPrivateAccess = true ) )
    UStaticMeshComponent *ProjectileMesh;

    UPROPERTY( VisibleAnywhere, BlueprintReadonly, Category = "Components", meta = ( AllowPrivateAccess = true ) )
    UParticleSystemComponent *ParticleTrail;

private: // Variables --------------------------------------------------------------------------------------------------
    UPROPERTY( EditDefaultsOnly, Category = "Damage" )
    TSubclassOf<UDamageType> DamageType;

    UPROPERTY( EditAnywhere, BlueprintReadonly, Category = "Movement", meta = ( AllowPrivateAccess = true ) )
    float MovementSpeed = 1300.f;

    UPROPERTY( EditAnywhere, BlueprintReadonly, Category = "Damage", meta = ( AllowPrivateAccess = true ) )
    float DamageAmount = 50.f;

    UPROPERTY( EditAnywhere, Category = "Effects" )
    UParticleSystem *HitParticle;

    UPROPERTY( EditAnywhere, Category = "Effects" )
    USoundBase *HitSound;

    UPROPERTY( EditAnywhere, Category = "Effects" )
    USoundBase *LaunchSound;

    UPROPERTY( EditAnywhere, Category = "Effects" )
    TSubclassOf<UMatineeCameraShake> HitShake;

private: // Functions --------------------------------------------------------------------------------------------------
    UFUNCTION()
    void OnHit(
        UPrimitiveComponent *HitComponent,
        AActor *             OtherActor,
        UPrimitiveComponent *OtherComponent,
        FVector              NormalImpulse,
        const FHitResult &   Hit );
};
