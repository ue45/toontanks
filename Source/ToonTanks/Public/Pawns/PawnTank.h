// Copyright (C) 2021 Oleksandr Manenko. All rights reserved.

#pragma once

#include "Components/AudioComponent.h"
#include "CoreMinimal.h"
#include "Pawns/PawnBase.h"

#include "PawnTank.generated.h"

class USpringArmComponent;
class UCameraComponent;
class APlayerController;
class UAudioComponent;

UCLASS()
class TOONTANKS_API APawnTank: public APawnBase
{
    GENERATED_BODY()

public:
    APawnTank();
    void SetupPlayerInputComponent( UInputComponent *PlayerInputComponent ) override;
    void Tick( float DeltaTime ) override;
    void HandleDestruction() override;
    bool GetIsAlive() const;

protected:
    void BeginPlay() override;

private:
    void CalculateMoveInput( float Value );
    void CalculateTurnInput( float Value );

    void OnStartMoving();
    void OnStopMoving();

    void UpdateMovement();

    bool HasMovement();

    void Move();
    void Turn();

private: // Components -------------------------------------------------------------------------------------------------
    UPROPERTY( VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = ( AllowPrivateAccess = "true" ) )
    USpringArmComponent *SpringArm;

    UPROPERTY( VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = ( AllowPrivateAccess = "true" ) )
    UCameraComponent *Camera;

    UPROPERTY( EditAnywhere, BlueprintReadWrite, Category = "Movement", meta = ( AllowPrivateAccess = "true" ) )
    float MoveSpeed = 200.f;

    UPROPERTY( EditAnywhere, BlueprintReadWrite, Category = "Movement", meta = ( AllowPrivateAccess = "true" ) )
    float TurnSpeed = 100.f;

    UPROPERTY()
    UAudioComponent *EngineAudio;

private: // Variables --------------------------------------------------------------------------------------------------
    UPROPERTY( EditAnywhere, Category = "Effects" )
    USoundBase *EngineSound;

private:
    APlayerController *PlayerController;

    bool bIsAlive = true;
    bool bMoving  = false;

    FVector MoveDirection;
    FQuat   TurnDirection;
};
