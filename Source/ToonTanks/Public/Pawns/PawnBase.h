// Copyright (C) 2021 Oleksandr Manenko. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Engine/EngineTypes.h"
#include "GameFramework/Pawn.h"

#include "PawnBase.generated.h"

class AProjectileBase;
class UCapsuleComponent;
class USceneComponent;
class USoundBase;
class UStaticMeshComponent;
class UTTHealthComponent;

UCLASS()
class TOONTANKS_API APawnBase: public APawn
{
    GENERATED_BODY()

public:
    APawnBase();

    virtual void HandleDestruction();

protected:
    void RotateTurret( FVector LookAtTarget );
    void Fire();

private: // Components -------------------------------------------------------------------------------------------------
    UPROPERTY( VisibleAnywhere, BlueprintReadonly, Category = "Components", meta = ( AllowPrivateAccess = "true" ) )
    UCapsuleComponent *Capsule;

    UPROPERTY( VisibleAnywhere, BlueprintReadonly, Category = "Components", meta = ( AllowPrivateAccess = "true" ) )
    UStaticMeshComponent *BaseMesh;

    UPROPERTY( VisibleAnywhere, BlueprintReadonly, Category = "Components", meta = ( AllowPrivateAccess = "true" ) )
    UStaticMeshComponent *TurretMesh;

    UPROPERTY( VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = ( AllowPrivateAccess = "true" ) )
    USceneComponent *ProjectileSpawnPoint;

    UPROPERTY( VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = ( AllowPrivateAccess = "true" ) )
    UTTHealthComponent *Health;

private: // Variables --------------------------------------------------------------------------------------------------
    UPROPERTY( EditAnywhere, BlueprintReadOnly, Category = "Projectile", meta = ( AllowPrivateAccess = "true" ) )
    TSubclassOf<AProjectileBase> ProjectileClass;

    UPROPERTY( EditAnywhere, Category = "Effects" )
    UParticleSystem *DeathParticle;

    UPROPERTY( EditAnywhere, Category = "Effects" )
    USoundBase *DeathSound;

    UPROPERTY( EditAnywhere, Category = "Effects" )
    TSubclassOf<UMatineeCameraShake> DeathShake;
};
