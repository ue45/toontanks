// Copyright (C) 2021 Oleksandr Manenko. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Pawns/PawnBase.h"

#include "PawnTurret.generated.h"

class APawnTank;

UCLASS()
class TOONTANKS_API APawnTurret: public APawnBase
{
    GENERATED_BODY()

public:
    void Tick( float DeltaTime ) override;
    void HandleDestruction() override;

protected:
    void BeginPlay() override;

private:
    void CheckFireCondition();

    bool IsPlayerInFireRange();

private:
    UPROPERTY( EditAnywhere, BlueprintReadWrite, Category = "Combat", meta = ( AllowPrivateAccess = "true" ) )
    float FireRate = 1.0;

    UPROPERTY( EditAnywhere, BlueprintReadWrite, Category = "Combat", meta = ( AllowPrivateAccess = "true" ) )
    float FireRange = 500.0;

    FTimerHandle FireRateTimerHandle;
    APawnTank *  PlayerPawn;
};
