// Copyright (C) 2021 Oleksandr Manenko. All rights reserved.

#pragma once

#include "Components/ActorComponent.h"
#include "CoreMinimal.h"

#include "TTHealthComponent.generated.h"

class ATTGameModeBase;

UCLASS( ClassGroup = ( Custom ), meta = ( BlueprintSpawnableComponent ) )
class TOONTANKS_API UTTHealthComponent: public UActorComponent
{
    GENERATED_BODY()

public:
    UTTHealthComponent();

protected:
    void BeginPlay() override;

    UFUNCTION()
    void TakeDamage(
        AActor *           DamageActor,
        float              DamageAmount,
        const UDamageType *DamageType,
        AController *      InstigatedBy,
        AActor *           DamageCauser );

private:
    UPROPERTY( EditAnywhere, BlueprintReadonly, meta = ( AllowPrivateAccess = "true" ) )
    float DefaultHealth = 100.f;

    UPROPERTY( BlueprintReadonly, meta = ( AllowPrivateAccess = "true" ) )
    float Health = 0.f;

private:
    ATTGameModeBase *GameModeRef;
};
